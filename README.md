
### Repostitory Details ###

This is the repository for the Agent Managment System (working title).

### Contribution guidelines ###

* Fork repository
* Create new branch
* Implement features & tests
* Push to fork
* Create pull request

### Other Info ###

* Please make sure of clean and readable code (with concise comments)

Copyright, 2016, __Radyin Technologies__
